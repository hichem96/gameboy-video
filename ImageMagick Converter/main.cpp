/*Code to read a video and save it frame-by-frame: https://gist.github.com/itsrifat/66b253db2736b23f247c*/

#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include "opencv2/video.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"

using namespace std;
using namespace cv;

string videoPath = "Videos/";
string framesPath = "Frames/";
string filename = videoPath + "shrektd.mp4";
int frameCount = 0;


int main()
{
    // Load the video
    VideoCapture cap(filename);
    Mat img;

    if(!cap.isOpened()){
        cout << "Error: Could not open " + filename;
        return -1;
    }

    while(true){
        // Get the frame
        cap.read(img);
        // Save the frame in the Frames folder
        imwrite(framesPath + "img" + to_string(frameCount) + ".png", img);
        cout << framesPath + "img" + to_string(frameCount) + ".png" << endl;
        frameCount++;
        if(img.empty()){
            break;
        }

        //Press ESC to exit
        char c = (char)waitKey(27);
        if(c == 27){
            break;
        }
    }

    system("convert-image.sh");

    return 0;
}