cmake_minimum_required(VERSION 3.10)

# set the project name
project(Tutorial)

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )

# add the executable
add_executable(Tutorial main.cpp)
target_link_libraries( Tutorial ${OpenCV_LIBS} )
