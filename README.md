# Gameboy Video

This is a test project to see if it's possible to play videos on the Gameboy. You will need ImageMagick, OpenCV and GBDK. This has been tested on linux, but can probably be tweaked to work on Windows.

STEP 1:
    - In the ImageMagick Converter folder, save the video you want to convert in the Videos folder.
    - In ImageMagick/main.cpp, you can replace "shrektd.mp4" by the name of the video in the Videos folder on line 16.
    - To compile, run the command make.
    - To execute, run ./ImageParser which will save every frames of the video in the Frames folder.

STEP 2:
    - In the convert-image.sh, you can set the size on line 16 (max of 160x144).
    - The script takes the frames in the Frames folder, resize it, grayscale it and index it to 4-bits colors for use on the Gameboy. It will then save a data.c and map.c file for every frames. Everything is saved in the Output folder.

STEP 3:
    - In the gameboy-video folder, for every sprites to use, put the data.c and map.c in their corresponding folders. You can the include them and use them.
    - If the number of tiles is over 255, you won't be able to import it.

Resources:
    - OpenCV on linux: https://docs.opencv.org/4.x/d7/d9f/tutorial_linux_install.html
    - ImageMagick: https://www.imagemagick.org/script/install-source.php
    - How the code in gameboy-video/main.c works: https://www.youtube.com/watch?v=QetQUo6GGD4&t=655s