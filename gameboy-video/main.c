#include <gb/gb.h>
#include <stdio.h>
#include "Sprites/smile.c"
#include "Tileset/backgroundTiles.c"
#include "Tileset/backgroundMap.c"
// Maps
#include "Map/img6_map.c"
#include "Map/img7_map.c"
#include "Map/img8_map.c"
#include "Map/img9_map.c"
#include "Map/img10_map.c"
#include "Map/img11_map.c"
#include "Map/img12_map.c"

// Data
#include "Data/img6_data.c"
#include "Data/img7_data.c"
#include "Data/img8_data.c"
#include "Data/img9_data.c"
#include "Data/img10_data.c"
#include "Data/img11_data.c"
#include "Data/img12_data.c"

void main(){
    // Time to wait in between frames
    int t = 1;
    // Center the image (Do once). Full screen image would be 160x144, but I used 160x136
    scroll_bkg(0, -4);
    while(1){
        //Frame #6
        // In the data.c file of the frame, replace 246 by corresponding number of tiles (must be under 255)
        set_bkg_data(0, 246, img6_data);
        //In the map.c file 20 and 17 corresponds to the size
        set_bkg_tiles(0, 0, 20, 17, img6_map);
        SHOW_BKG;
        DISPLAY_ON;
        delay(t);
        //Frame #7
        set_bkg_data(0, 251, img7_data);
        set_bkg_tiles(0, 0, 20, 17, img7_map);
        SHOW_BKG;
        DISPLAY_ON;
        delay(t);
        //Frame #8
        set_bkg_data(0, 251, img8_data);
        set_bkg_tiles(0, 0, 20, 17, img8_map);
        SHOW_BKG;
        DISPLAY_ON;
        delay(t);
        //Frame #9
        set_bkg_data(0, 251, img9_data);
        set_bkg_tiles(0, 0, 20, 17, img9_map);
        SHOW_BKG;
        DISPLAY_ON;
        delay(t);
        //Frame #10
        set_bkg_data(0, 252, img10_data);
        set_bkg_tiles(0, 0, 20, 17, img10_map);
        SHOW_BKG;
        DISPLAY_ON;
        delay(t);
        //Frame #11
        set_bkg_data(0, 250, img11_data);
        set_bkg_tiles(0, 0, 20, 17, img11_map);
        SHOW_BKG;
        DISPLAY_ON;
        delay(t);
        //Frame #12
        set_bkg_data(0, 251, img12_data);
        set_bkg_tiles(0, 0, 20, 17, img12_map);
        SHOW_BKG;
        DISPLAY_ON;
        delay(t);
    }
    



}

