#!/bin/bash
# Script to read a png, resize it, grayscale it and index it to 4 bits of colors
FILES="*.png"
INPUT="Frames/"
OUTPUT="Output/"

# Take the frames extracted from the video and convert them in a format that will be converted into C
for file in "Frames/"$FILES
do
    echo "Processing" "${file//$INPUT/}"
    magick convert "$file" -resize 160x136\! -type grayscale -colors 4 "$OUTPUT${file//$INPUT/}"
done
echo "Done"

# Take the converted frames and convert them into C files
for images in $OUTPUT$FILES
do
    # ../linux-x64/./GameBoyPngConverter "${images//$OUTPUT/}"
    yes | ../linux-x64/./GameBoyPngConverter $images
    echo ""
done
echo "Done"

